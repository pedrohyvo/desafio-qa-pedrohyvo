import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class CheckOutTest {

    @Test
    public void totalsTest() {
        assertEquals(0, calculatePrice(""));
        assertEquals(50, calculatePrice("A"));
        assertEquals(80, calculatePrice("AB"));
        assertEquals(115, calculatePrice("CDBA"));

        assertEquals(100, calculatePrice("AA"));
        assertEquals(130, calculatePrice("AAA"));
        assertEquals(180, calculatePrice("AAAA"));
        assertEquals(230, calculatePrice("AAAAA"));
        assertEquals(260, calculatePrice("AAAAAA"));

        assertEquals(160, calculatePrice("AAAB"));
        assertEquals(175, calculatePrice("AAABB"));
        assertEquals(190, calculatePrice("AAABBD"));
        assertEquals(190, calculatePrice("DABABA"));

        //More total tests
        assertEquals(310, calculatePrice("DABCDABCDABC"));
        assertEquals(185, calculatePrice("ABCCAB"));
        assertEquals(145, calculatePrice("AABB"));
        assertEquals(45, calculatePrice("BB"));
        assertEquals(40, calculatePrice("CC"));
        assertEquals(35, calculatePrice("CD"));
        assertEquals(65, calculatePrice("BCD"));
    }

    @Test
    public void incrementalTest() {
        CheckOut checkOut = new CheckOut(givenPriceRuleSet());
        assertEquals(0, checkOut.total());
        checkOut.add("A");
        assertEquals(50, checkOut.total());
        checkOut.add("B");
        assertEquals(80, checkOut.total());
        checkOut.add("A");
        assertEquals(130, checkOut.total());
        checkOut.add("A");
        assertEquals(160, checkOut.total());
        checkOut.add("B");
        assertEquals(175, checkOut.total());
    }

    /**This method is responsible to get item price
    *@param items skus
    *@return price total price (integer value)
    */
    public int calculatePrice(String items) {
        CheckOut checkOut = new CheckOut(givenPriceRuleSet());
        for (int i = 0; i < items.length(); i++) {
            checkOut.add(String.valueOf(items.charAt(i)));
        }
        return checkOut.total();
    }

    //Method to set the item rules
    private SpecialPriceRule givenPriceRuleSet() {
        //create a map that relates the item to the price
        Map<String, SpecialPrice> priceRules = new HashMap<>();
        //A - unit price = 50 and 3 items = 130
        priceRules.put("A", new SpecialPrice(50, 3, 130));
        //B - unit price = 30 and 2 items = 45
        priceRules.put("B", new SpecialPrice(30, 2, 45));
        //C - unit price = 20
        priceRules.put("C", new SpecialPrice(20));
        //D - unit price = 15
        priceRules.put("D", new SpecialPrice(15));
        return new SpecialPriceRule(priceRules);
    }
}