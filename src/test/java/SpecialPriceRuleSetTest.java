import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class SpecialPriceRuleSetTest {

    @Test
    public void noUnitPriceTest() {
        Map<String, SpecialPrice> priceRules = new HashMap<>();
        priceRules.put("A", new SpecialPrice(0));

        SpecialPriceRule specialPriceRule = new SpecialPriceRule(priceRules);

        assertThat(specialPriceRule.calculateTotalPrice(asList("A")), is(0));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A")), is(0));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A", "A")), is(0));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A", "A", "A")), is(0));
    }

    @Test
    public void unitPriceTest() {
        Map<String, SpecialPrice> priceRules = new HashMap<>();
        priceRules.put("A", new SpecialPrice(15));

        SpecialPriceRule specialPriceRule = new SpecialPriceRule(priceRules);

        assertThat(specialPriceRule.calculateTotalPrice(asList("A")), is(15));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A")), is(30));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A", "A")), is(45));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A", "A", "A")), is(60));
    }

    @Test
    public void specialPriceTest() {
        Map<String, SpecialPrice> priceRules = new HashMap<>();
        priceRules.put("A", new SpecialPrice(20, 3, 50));

        SpecialPriceRule specialPriceRule = new SpecialPriceRule(priceRules);

        assertThat(specialPriceRule.calculateTotalPrice(asList("A")), is(20));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A")), is(40));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A", "A")), is(50));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A", "A", "A")), is(70));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A", "A", "A", "A", "A")), is(90));
    }

    @Test
    public void twoSpecialPriceTest() {
        Map<String, SpecialPrice> priceRules = new HashMap<>();
        priceRules.put("A", new SpecialPrice(5, 3, 10));
        priceRules.put("B", new SpecialPrice(10, 2, 15));

        SpecialPriceRule specialPriceRule = new SpecialPriceRule(priceRules);

        assertThat(specialPriceRule.calculateTotalPrice(asList("A","B")), is(15));
        assertThat(specialPriceRule.calculateTotalPrice(asList("B","B")), is(15));
        assertThat(specialPriceRule.calculateTotalPrice(asList("B","B","B")), is(25));
        assertThat(specialPriceRule.calculateTotalPrice(asList("B","B","B","B")), is(30));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A","A")), is(10));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A","A","A")), is(10));
        assertThat(specialPriceRule.calculateTotalPrice(asList("A","A","A","A")), is(15));
    }

}