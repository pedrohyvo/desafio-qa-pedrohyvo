import java.util.LinkedList;
import java.util.List;

/**
 * CheckOut.java - a simple class for checkout model.
 * @author  Pedro Hyvo
 */
public class CheckOut {

    private final PriceRuleI ruleGroup;
    private List<String> skus = new LinkedList<>();

    public CheckOut(PriceRuleI ruleGroup) {
        this.ruleGroup = ruleGroup;
    }

    public void add(String s) {
        skus.add(s);
    }

    public int total() {
        return ruleGroup.calculateTotalPrice(skus);
    }
}

