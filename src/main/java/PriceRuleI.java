import java.util.Collection;

public interface PriceRuleI {

    int calculateTotalPrice(Collection<String> skus);

}
