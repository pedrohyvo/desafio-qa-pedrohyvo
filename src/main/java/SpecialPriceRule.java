import java.util.Collection;
import java.util.Map;

import static java.util.stream.Collectors.*;

/**
 * SpecialPriceRule.java - a class responsible for business model
 * @author  Pedro Hyvo
 */
public class SpecialPriceRule implements PriceRuleI {

    private Map<String, SpecialPrice> priceRules;

    public SpecialPriceRule(Map<String, SpecialPrice> priceRules) {
        this.priceRules = priceRules;
    }

    /**
     * Method responsible for collecting the frequencies of each value
     * and making the total calculation of the sum;
     * @param skus collection of skus
     * @return result of the sum (integer value)
     */
    @Override
    public int calculateTotalPrice(Collection<String> skus) {
        return skus.stream()
                .collect(groupingBy(x -> x, counting()))
                .entrySet().stream()
                .map(x -> calculatePriceForSKU(x.getKey(), x.getValue()))
                .mapToInt(Integer::intValue)
                .sum();
    }

    /**Method to calculate price for each SKU
    *@param sku SKU
    *@param quantity quantity
    *@return total price for SKU
    */
    private int calculatePriceForSKU(String name, long quantity) {
        SpecialPrice price = priceRules.get(name);

        //get special quantity
        int specialBatchSize = price.getQuantity();
        //get special price
        int specialPrice = price.getBatchPrice();
        //get unit price
        int unitPrice = price.getUnitPrice();

        //If Item has quantity defined on rule
        int batchCount = (int) (quantity / specialBatchSize);
        int remainder = (int) (quantity % specialBatchSize);
        //Calculate if the item has special price plus quantity * unit price
        return (batchCount * specialPrice) + (remainder * unitPrice);
    }
}
