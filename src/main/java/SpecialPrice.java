/**
 * SpecialPrice.java - a simple class for special price model.
 * A representation of the two types of SpecialPrice proposed overloading the constructor.
 * Price per unit and price per lot.
 * @author  Pedro Hyvo
 */
public class SpecialPrice {

    private int unitPrice;
    private int quantity;
    private int batchPrice;

    public SpecialPrice(int unitPrice, int quantity, int batchPrice) {
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.batchPrice = batchPrice;
    }

    public SpecialPrice(int unitPrice) {
        this.unitPrice = unitPrice;
        this.quantity = 1;
        this.batchPrice = unitPrice;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getBatchPrice() {
        return batchPrice;
    }
}
