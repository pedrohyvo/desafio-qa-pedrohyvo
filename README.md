# Kata09 Pedro

Utilizei a linguagem JAVA, pois é a linguagem que tenho maior experiência e segurança para codificar.

## Task 1
* Criei o arquivo 'specs-task1' para colocar a task 1 com Gherkin;

## Solução Kata09

 * Para resolver o problema, pensei em pegar a quantidade total de cada letra.
    * Ex: AABC - 2 A, 1 B, 1 C
 * Após fazer o agrupamento, coloquei os valores dentro de um Mapa(valor e quantidade);
   Por fim, para calcular o preço final seria:
    * quantidadeDeLotes * PreçoEspecialPorLote + quantidadeRestante * preçoUnitário.

## Testes

* Copiei a classe de teste e acrescentei algumas asserções;
* Criei uma classe de teste para testar a implementação do preço especial;

```
Desde já agradeço a oportunidade de participar no processo
de seleção e espero que gostem do meu trabalho. :)
```

